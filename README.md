```
██╗    ██╗███████╗██╗      ██████╗ ██████╗ ███╗   ███╗███████╗    ████████╗ ██████╗     ███╗   ███╗██╗   ██╗
██║    ██║██╔════╝██║     ██╔════╝██╔═══██╗████╗ ████║██╔════╝    ╚══██╔══╝██╔═══██╗    ████╗ ████║╚██╗ ██╔╝
██║ █╗ ██║█████╗  ██║     ██║     ██║   ██║██╔████╔██║█████╗         ██║   ██║   ██║    ██╔████╔██║ ╚████╔╝ 
██║███╗██║██╔══╝  ██║     ██║     ██║   ██║██║╚██╔╝██║██╔══╝         ██║   ██║   ██║    ██║╚██╔╝██║  ╚██╔╝  
╚███╔███╔╝███████╗███████╗╚██████╗╚██████╔╝██║ ╚═╝ ██║███████╗       ██║   ╚██████╔╝    ██║ ╚═╝ ██║   ██║   
 ╚══╝╚══╝ ╚══════╝╚══════╝ ╚═════╝ ╚═════╝ ╚═╝     ╚═╝╚══════╝       ╚═╝    ╚═════╝     ╚═╝     ╚═╝   ╚═╝   
                                                                                                            
 ██████╗ ██╗████████╗██╗  ██╗██╗   ██╗██████╗     ██████╗ ██████╗  ██████╗ ███████╗██╗██╗     ███████╗      
██╔════╝ ██║╚══██╔══╝██║  ██║██║   ██║██╔══██╗    ██╔══██╗██╔══██╗██╔═══██╗██╔════╝██║██║     ██╔════╝      
██║  ███╗██║   ██║   ███████║██║   ██║██████╔╝    ██████╔╝██████╔╝██║   ██║█████╗  ██║██║     █████╗        
██║   ██║██║   ██║   ██╔══██║██║   ██║██╔══██╗    ██╔═══╝ ██╔══██╗██║   ██║██╔══╝  ██║██║     ██╔══╝        
╚██████╔╝██║   ██║   ██║  ██║╚██████╔╝██████╔╝    ██║     ██║  ██║╚██████╔╝██║     ██║███████╗███████╗      
 ╚═════╝ ╚═╝   ╚═╝   ╚═╝  ╚═╝ ╚═════╝ ╚═════╝     ╚═╝     ╚═╝  ╚═╝ ╚═════╝ ╚═╝     ╚═╝╚══════╝╚══════╝      
 ```

# About Me
Hiyah, 👋 I am Saif, a student, a son, a husband. Supporter && contributor of free && open source community. Believes in free as in freedom.
- 🌐 Website:      https://saifshahriar.github.io/
- 😺 GitHub:		https://github.com/saifshahriar
- 🦊 GitLab:			https://gitlab.com/saifshahriar
- 🗾 Country:				Bangladesh 🇧🇩
- ⚡ Skilled in:     Linux, cyber security, admin.
- ㊗ Language Proficiency:

| Language      | READ 📖 | WRITE ✍ | SPEAK 🗣 |
|---------------|---------|---------|---------|
| English       | ✓       | ✓       | ✓       |
| Bengali       | ✓       | ✓       | ✓       |
| Hindi && Urdu | ✓       | ✓       | ✓       |
| Arabic        | ✓       | ✓       |         |

- 👨‍💻 Programming Language:
<!---
 	- C,
	- **Shell** (POSIX && Bash), 
	- Python, 🐍 
	- Go, 🐹
	- Lua, 🌙
	- Rust. 🦀
  - Others: 
  	- HTML, 
  	- XML, 
  	- CSS.
---> 

  ![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=saifshahriar&langs_count=20&layout=compact&exclude_repo=saifshahriar.github.io&hide=javascript,css&theme=tokyonight)
- 💻 OS:
	- Linux && BSD.
- 📒 Editor/IDE:
	- **Vim**.
	- **Emacs**.
	- Visual Studio Code.
- 📈 Projects:
	- **ArchnixOS** (an <i>Arch Linux</i> based OS - [Under Development])
	- dwm (dynamic window manager)
		- A tiling window manager for Linux and BSD.
		- Fork of suckless.org's dwm.
	- st (simple terminal)
		- A terminal emmulator for Linux and BSD.
		- Fork of suckless.org's st.
	- dmenu (a run launcher)
		- Fork of suckless.org's dmenu.
	- isurpcon

	[![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=saifshahriar&repo=isurpcon&show_icons=true&theme=tokyonight)](https://github.com/saifshahriar/isurpcon)


		




<!---
- 👋 Hi, I’m saifshahriar, an I.Sc. student.
- 👀 I’m interested in learning new skills.
- 🌱 I’m currently learning JS and Data Science.
- 💞️ I’m looking to collaborate on ...
- 📫 How to reach me ...
--->
<!---
saifshahriar/saifshahriar is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
-------------------------------------------------------------------------------
![Saif's GitHub stats](https://github-readme-stats.vercel.app/api?username=saifshahriar&show_icons=true&theme=tokyonight)

-------------------------------------------------------------------------------
_This document was written using emacs._
